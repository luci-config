from recipe_engine.post_process import MustRun, DoesNotRun

PYTHON_VERSION_COMPATIBILITY = "PY3"

# Reference: https://chromium.googlesource.com/infra/luci/recipes-py/+/HEAD/doc/

# All the deps get combined into an `api` variable passed in to RunSteps and
# GenTests. Versions and urls are specified in `recipes.cfg`.
DEPS = [
    'depot_tools/gerrit',
    'depot_tools/gsutil',
    'infra/zip',
    'fuchsia/git_checkout',
    'recipe_engine/buildbucket',
    'recipe_engine/context',
    'recipe_engine/file',
    'recipe_engine/json',
    'recipe_engine/nodejs',
    'recipe_engine/path',
    'recipe_engine/step',
]


def RunSteps(api):
    # The code for the change will be checked out into a special folder that is
    # cached between builds.
    checkout_root = api.path.cache_dir / 'builder'
    # This is the name of the subfolder under checkout_root where the change will
    # be checked out at.
    checkout_dir = 'src'

    with api.context(cwd=checkout_root):
        api.git_checkout(
            'https://gerrit.googlesource.com/luci-test',
            path=checkout_root / checkout_dir,
        )

    env = _downloadChromeAndAddToPath(api)

    # Now run our verification in the checked out code directory.
    with api.context(env=env, cwd=checkout_root / checkout_dir):
        # Current LTS for Node.js. Version must be in CIPD packages:
        # https://chrome-infra-packages.appspot.com/p/infra/3pp/tools/nodejs/linux-amd64/+/bfm5RWHxZDS1NBdMuNLvXZUq4ULnf_Ho5OzhI3jL_14C
        with api.nodejs(version='18.11.0'):
            # Named steps to test the change.
            api.step('install deps', ['npm', 'install'])
            api.step('run tests', ['npm', 'run', 'test'])

# Download and unzip Chrome from a cloud storage bucket, then add it to the PATH
# and rename it for compatibility with our test runner:
# https://github.com/GoogleChrome/chrome-launcher#launch-options
# Adapted from:
# https://chromium.googlesource.com/infra/infra/+/refs/heads/main/recipes/recipes/gerrit_plugins.py


def _downloadChromeAndAddToPath(api):
    with api.step.nest('get chrome'):
        chrome = api.path.mkdtemp(prefix='chrome')
        gs_bucket = 'chromium-browser-snapshots'
        gs_path = 'Linux_x64'
        version_file = 'LAST_CHANGE'
        chrome_zip = 'chrome-linux.zip'
        api.gsutil.download(gs_bucket, '%s/%s' %
                            (gs_path, version_file), chrome)
        version = api.file.read_text('read latest chrome version',
                                     chrome / version_file)
        api.gsutil.download(gs_bucket, '%s/%s/%s' %
                            (gs_path, version, chrome_zip), chrome)
        api.zip.unzip('unzip chrome', chrome / chrome_zip, chrome / 'zip')
        chrome_path = chrome / 'zip' / 'chrome-linux'

        # The chrome-launcher package expects a binary named "google-chrome" rather
        # than "chrome":
        # https://github.com/GoogleChrome/chrome-launcher/blob/c753ba083c43a90d98b682bf9ffdfe21680e6208/src/chrome-finder.ts#L133
        chrome_bin = chrome_path / 'chrome'
        google_chrome_bin = chrome_path / 'google-chrome'
        api.step('rename to google-chrome',
                 ['mv', chrome_bin, google_chrome_bin])
        env = {
            'PATH': api.path.pathsep.join([str(chrome_path), '%(PATH)s']),
        }
        return env

# Test the recipe and generate the expect json file.


def GenTests(api):
    yield api.test(
        'basic',
        # Execute RunSteps with test data
        api.buildbucket.try_build(
            project="gerrit",
            git_repo="https://gerrit.googlesource.com/luci-test",
        ),
        api.post_process(MustRun, 'run tests'),
    )

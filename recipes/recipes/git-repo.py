# Copyright (C) 2023 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

DEPS = [
    'recipe_engine/buildbucket',
    'recipe_engine/step',
    'recipe_engine/path',
    'recipe_engine/context',
    'depot_tools/bot_update',
    'depot_tools/gclient',
]

PYTHON_VERSION_COMPATIBILITY = 'PY3'


def gclient_config(api):
    """Generate a gclient configuration to check out git-repo.

  Return: (config) A gclient recipe module configuration.
  """
    cfg = api.gclient.make_config()
    soln = cfg.solutions.add()
    soln.name = 'git-repo'
    soln.url = 'https://gerrit.googlesource.com/git-repo'
    soln.revision = 'HEAD'
    return cfg


def RunSteps(api):
    cl = api.buildbucket.build.input.gerrit_changes[0]
    project_name = cl.project
    assert project_name == 'git-repo', 'unknown project: "%s"' % project_name

    api.bot_update.ensure_checkout(gclient_config=gclient_config(api))

    # TODO(b/266734831): Find out why smoke tests fail.
    # TODO(b/266734831): Find out why superproject tests take more than 8m each.
    tests_to_skip = [
        'test_smoke_repo',
        'test_smoke_git',
        'test_superproject_get_superproject_invalid_branch',
        'test_superproject_get_superproject_invalid_url',
    ]
    expr = ' and '.join(['not ' + t for t in tests_to_skip])

    with api.context(cwd=api.path.start_dir / project_name):
        # TODO(b/266734831): Use tox to run tests.
        api.step('Run tests', ['vpython3', 'run_tests', '-k', expr, '-vv'])


def GenTests(api):
    yield (api.test('basic') + api.buildbucket.try_build(project='git-repo'))

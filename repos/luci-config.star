# This is configuration for LUCI for the luci-config repo
# https://gerrit.googlesource.com/luci-config

# Create a CQ group to watch luci-config repo for changes. No recipe is
# configured so changes will just be merged if they have Bot-Commit+1 or
# Code-Review+2 and then CQ+2 is set.
luci.cq_group(
    name = "luci-config_repo",
    acls = [
        # Everyone can submit by voting CQ+2 since rights to vote CQ+2 are limited
        # by Gerrit
        acl.entry(
            acl.CQ_COMMITTER,
            groups = "all",
        ),
    ],
    watch = cq.refset(
        repo = "https://gerrit.googlesource.com/luci-config",
        refs = ["refs/heads/main"],
    ),
)

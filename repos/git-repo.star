# This is configuration for LUCI for the git-repo repo
# https://gerrit.googlesource.com/git-repo

# Set up Swarming permissions (for LED).
luci.binding(
    realm = "try",
    roles = "role/swarming.taskTriggerer",
    groups = "mdb/chrome-ops-source",
)

luci.binding(
    realm = "pools/try",
    roles = "role/swarming.poolUser",
    groups = "mdb/chrome-ops-source",
)

luci.builder(
    name = "Verify git-repo CL",
    bucket = "try",
    executable = luci.recipe(
        name = "git-repo",
    ),
    service_account = "gerrit-luci-try-builder@gerritcodereview-ci.iam.gserviceaccount.com",
    dimensions = {
        "os": "Ubuntu",
        "cpu": "x86-64",
        "pool": "luci.gerrit.try",
    },
)

luci.cq_group(
    name = "git-repo_repo",
    acls = [
        acl.entry(
            roles = [acl.CQ_DRY_RUNNER, acl.CQ_NEW_PATCHSET_RUN_TRIGGERER],
            groups = "all",
        ),
        acl.entry(
            acl.CQ_COMMITTER,
            groups = "googlers",
        ),
    ],
    watch = cq.refset(
        repo = "https://gerrit.googlesource.com/git-repo",
        refs = ["refs/heads/.+"],
    ),
)

luci.cq_tryjob_verifier(
    builder = "try/Verify git-repo CL",
    cq_group = "git-repo_repo",
    # Add NEW_PATCHSET_RUN to the defaults DRY_RUN and FULL_RUN
    mode_allowlist = [
        cq.MODE_DRY_RUN,
        cq.MODE_FULL_RUN,
        cq.MODE_NEW_PATCHSET_RUN,
    ],
)

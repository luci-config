# This is configuration for LUCI for the gerrit repo
# https://gerrit.googlesource.com/gerrit

# Builders

# Defines a builder to run the "gerrit" recipe in a given bucket. This
# will be hooked up to the CQ label rather than having a set schedule.
luci.builder(
    name = "Verify gerrit CL",
    bucket = "try",
    executable = luci.recipe(
        # The name of the recipe we just made.
        name = "gerrit",
    ),
    service_account = "gerrit-luci-try-builder@gerritcodereview-ci.iam.gserviceaccount.com",
    dimensions = {
        "os": "Ubuntu",
        "cpu": "x86-64",
        "pool": "luci.gerrit.try",
    },
)

# Create a CQ group to watch gerrit repo for changes.
luci.cq_group(
    name = "gerrit_repo",
    acls = [
        # Everyone can trigger dry runs by voting CQ+1
        acl.entry(
            acl.CQ_DRY_RUNNER,
            groups = "all",
        ),
        # Everyone can Trigger dry runs by uploading a new patchset
        acl.entry(
            acl.CQ_NEW_PATCHSET_RUN_TRIGGERER,
            groups = "all",
        ),
        # Every CQ+2 should be able to submit since rights to vote CQ+2 are
        # well-controlled on the Gerrit side.
        acl.entry(
            acl.CQ_COMMITTER,
            groups = "all",
        ),
    ],
    watch = cq.refset(
        repo = "https://gerrit.googlesource.com/gerrit",
        refs = ["refs/heads/master"],
    ),
    post_actions = [
        cq.post_action_gerrit_label_votes(
            name = "success",
            conditions = [
                cq.post_action_triggering_condition(
                    mode = cq.MODE_DRY_RUN,
                    statuses=[cq.STATUS_SUCCEEDED],
                ),
                cq.post_action_triggering_condition(
                    mode = cq.MODE_NEW_PATCHSET_RUN,
                    statuses=[cq.STATUS_SUCCEEDED],
                ),
            ],
            labels = {"Frontend-Verified": 1},
        ),
        cq.post_action_gerrit_label_votes(
            name = "failure",
            conditions = [
                cq.post_action_triggering_condition(
                    mode = cq.MODE_DRY_RUN,
                    statuses=[cq.STATUS_FAILED, cq.STATUS_CANCELLED],
                ),
                cq.post_action_triggering_condition(
                    mode = cq.MODE_NEW_PATCHSET_RUN,
                    statuses=[cq.STATUS_FAILED, cq.STATUS_CANCELLED],
                ),
            ],
            labels = {"Frontend-Verified": -1},
        ),
    ],
)

# Attach our "Verify gerrit CL" builder to this CQ group.
luci.cq_tryjob_verifier(
    builder = "try/Verify gerrit CL",
    cq_group = "gerrit_repo",
    # Add NEW_PATCHSET_RUN to the defaults DRY_RUN and FULL_RUN
    mode_allowlist = [
        cq.MODE_DRY_RUN,
        cq.MODE_FULL_RUN,
        cq.MODE_NEW_PATCHSET_RUN,
    ],
    location_filters = [
        cq.location_filter(
            path_regexp = 'polygerrit-ui/.+'
        ),
    ],
)

# Show on https://ci.chromium.org/p/gerrit
luci.list_view(
    name = "gerrit",
    title = "gerrit builders",
    entries = [luci.list_view_entry(builder = "try/Verify gerrit CL")],
)

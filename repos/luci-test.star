# This is configuration for LUCI for the luci-test repo
# https://gerrit.googlesource.com/luci-test

# Builders

# Defines a builder to run the "luci-test" recipe in a given bucket. This will
# be hooked up to the CQ label rather than having a set schedule.
luci.builder(
    name = "Verify luci-test CL",
    bucket = "try",
    executable = luci.recipe(
        # The name of the recipe we just made.
        name = "luci-test",
    ),
    service_account = "gerrit-luci-try-builder@gerritcodereview-ci.iam.gserviceaccount.com",
    dimensions = {
        "os": "Ubuntu",
        "cpu": "x86-64",
        "pool": "luci.gerrit.try",
    },
)

# Create a CQ group to watch luci-test repo for changes.
luci.cq_group(
    name = "luci-test_repo",
    acls = [
        # Everyone can trigger dry runs by voting CQ+1
        acl.entry(
            acl.CQ_DRY_RUNNER,
            groups = "all",
        ),
        # Everyone can Trigger dry runs by uploading a new patchset
        acl.entry(
            acl.CQ_NEW_PATCHSET_RUN_TRIGGERER,
            groups = "all",
        ),
        # Only project-gerrit-committers can submit by voting CQ+2
        acl.entry(
            acl.CQ_COMMITTER,
            groups = "project-gerrit-committers",
        ),
    ],
    watch = cq.refset(
        repo = "https://gerrit.googlesource.com/luci-test",
        refs = ["refs/heads/.+"],  # will watch all branches
    ),
)

# Attach our "Verify luci-test CL" builder to this CQ group.
luci.cq_tryjob_verifier(
    builder = "try/Verify luci-test CL",
    cq_group = "luci-test_repo",
    # Add NEW_PATCHSET_RUN to the defaults DRY_RUN and FULL_RUN
    mode_allowlist = [
        cq.MODE_DRY_RUN,
        cq.MODE_FULL_RUN,
        cq.MODE_NEW_PATCHSET_RUN,
    ],
)

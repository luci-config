# LUCI Configuration

This repo holds all the configuration for LUCI, the CI system Gerrit uses. You
can find recent verification runs and logs at
https://ci.chromium.org/ui/p/gerrit/builders.

## Concepts

- `Recipe`: a python script to verify a change, and a test for that verification
- `Builder`: a recipe configured to run on a specific machine type
- `CQ Group`: a watcher that triggers on new votes/patchsets on the specified
repo+refs
- `CQ Tryjob Verifier`: combines a CQ group with a builder to run a recipe when
a new vote or patchset comes in

## Layout

- `/main.star`: overall starlark configuration for the entire host. It is also
executable to generate the config data
- `/repos/*.star`: repo-specific starlark configuration for builders, CQ groups,
CQ tryjob verifiers, etc
- `/recipes/recipes/*.py`: recipes for testing a change based on the repo
- `/recipes/recipes.py`: executes recipe tests and compares/generates expected
JSON goldens. 100% coverage is expected.
- `/recipes/recipe_modules/`: dependencies shared by modules
- `/recipes/*.expected/*.json`: expected commands executed by the recipe
- `/infra/config/recipes.cfg`: versions and URLs of external recipe dependencies

## Prerequisites

- `lucicfg` - CLI tool to generate the configuration from the starlark scripts.
To install, clone [depot_tools](https://chromium.googlesource.com/chromium/tools/depot_tools.git/).
- `python` - Python 3, used to execute recipes.

## Imporant commands

Regenerate config data after making changes (lucicfg):

> `$ ./main.star`

Run recipe tests (python):

> `$ recipes/recipes.py test run`

Update recipe test goldens (python):

> `$ recipes/recipes.py test train`

## Documentation

- [Configuration API](https://chromium.googlesource.com/infra/luci/luci-go/+/HEAD/lucicfg/doc/README.md)
- [Recipe User Guide](https://chromium.googlesource.com/infra/luci/recipes-py/+/HEAD/doc/user_guide.md)